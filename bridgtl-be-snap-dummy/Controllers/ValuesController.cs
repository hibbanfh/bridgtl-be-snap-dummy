﻿using bridgtl_be_snap_dummy.Models.bridgtl_be_snap_dummy.Models;
using bridgtl_be_snap_dummy.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Primitives;
using Newtonsoft.Json;

namespace bridgtl_be_snap_dummy.Controllers
{
    public class ValuesController : Controller
    {
        [HttpPost]
        [Route("snap/v1.0/access-token/b2b")]
        public async Task<IActionResult> CheckTokenConnection([FromBody] Grant grant)
        {
            Request.Headers.TryGetValue("X-SIGNATURE", out StringValues xSignature);
            Request.Headers.TryGetValue("X-CLIENT-KEY", out StringValues xKey);
            Request.Headers.TryGetValue("X-TIMESTAMP", out StringValues xTime);
            /*Console.WriteLine("X-SIGNATURE: " + xSignature);
            Console.WriteLine("X-CLIENT-KEY: " + xKey);
            Console.WriteLine("X-TIMESTAMP: " + xTime);
            Console.WriteLine("------");
            Console.WriteLine("Request: " + JsonConvert.SerializeObject(grant));*/

            Response res = new Response();
            res.accessToken = "BKOAY6Q6x67ekmwAvzt6RV8J9029";
            res.tokenType = "BearerToken";
            res.expiresIn = "899";

            return Ok(res);
        }

        [HttpPost]
        [Route("snap/v1.0/transfer-va/inquiry-intrabank")]
        public async Task<IActionResult> CheckInquiryIntrabank([FromBody] InquiryIntra inquiryIntra)
        {
            Request.Headers.TryGetValue("X-SIGNATURE", out StringValues xSignature);
            Request.Headers.TryGetValue("X-TIMESTAMP", out StringValues xTime);
            Request.Headers.TryGetValue("X-PARTNER-ID", out StringValues xPartner);
            Request.Headers.TryGetValue("CHANNEL-ID", out StringValues channelId);
            Request.Headers.TryGetValue("X-EXTERNAL-ID", out StringValues xExternalId);
            /*Console.WriteLine("X-SIGNATURE: " + xSignature);
            Console.WriteLine("X-TIMESTAMP: " + xTime);
            Console.WriteLine("X-PARTNER-ID: " + xPartner);
            Console.WriteLine("CHANNEL-ID: " + channelId);
            Console.WriteLine("X-EXTERNAL-ID: " + xExternalId);
            Console.WriteLine("------");
            Console.WriteLine("Request: " + JsonConvert.SerializeObject(inquiryIntra));*/

            string privateKey = "-----BEGIN RSA PRIVATE KEY-----\r\nMIIG5AIBAAKCAYEAugGR6vgduzYCI/lXaXDeJpsFBwzP5xwrzkiELvw2xSomqTY0\r\nERNpbtNAsnPjHB17lXzMVnEFOvCnw6Aa/fhYMC/UVraNVvq6NWaVjkOXVrx+hlCc\r\n27PvB89zvw54grCm0t3xB0QUSxgHgoyYQpE946HhDa6G2T/kygNjOBEVPD/aHTai\r\n1PkU+sn9jxSbsz4Icqi1wq+srrJISRhrMEXA2AmaoSXtOFSZdnnnksLRqooYLx9f\r\nhzAKlYBYEA5QNk2kwTdwNDO74mIiSydyftqOnx7MKOu0O1mpCfids0Bc2da1BaUI\r\n6eP/4DV3/OIwDcqrCdilciDAR2dmQD35ioTAmBXzRqv9L/GtmGB8ZSPreDGUF41C\r\narfKU6t4I1LD88UAjm1HIQU5FkE/Ca1nLS4fwciaq8NBP5Q1RVSEG4On7GdenUAT\r\nvv966OOKuR3RvvQKjuCdkeRzVBqU1qTEOtanuVlszSB4DQ4sTq3NVoxz3ZBmxKdR\r\nfAONIyoOxT3Bey7ZAgMBAAECggGBALO71qME872112WphzSk0pTdUINadv6r8wxW\r\neBvqrIXiUA9MCA3EAyoKycZmQrslJgjx2YVhmps0dfeLGunz8ktFvFm59mNozUvS\r\nf4tDdtZiN6RT+u41NTEH3aFtOzeVuO2jdkb+GtnhM0iL0IjqTbJK9uvAjB8bJVmq\r\n4lIH98wLKxj1dWxz7zjSu8dpM3aE56bbrR6doDACp+SxysmPbWOUDC3RNyMqZEve\r\nSc0/oVe4LtxB5Y389w01QmLpYcrHRFhSLhUwl2lTL5YccF1hts5H6MRUi8crSzkz\r\nJn/OTYJsSh0X9/TygQikcekMJPh1eQ3Nl7CR/h+B944rPDgSep1bSQsQcLyuO6nd\r\nF7qEcsDYplR7nCc01ExQHgwDwIka3boy9nalfnOtn67RAJdA8RTygEBREnM/dEit\r\n9vnbKLC5yvXw8no/UapKmVdROoyLZ0rddvx7mknbZdm9KosyH1wUsfDWAeKwM3Cm\r\nBYmvwQxrLdNAJ8HebUVaCzIynTXhgQKBwQDruXGXwrXtIOc/3tulLNINlQAyrSnU\r\nJ4NkVvXkMkV8Q7HQ4wLXGGf9X1q1NPZEVmV8Z86gyy4zTBEw4imWOEusOBIXFyGb\r\nmRWRJuuiCNicw8UKlQ/jWDUGFhNj6SXf6eSBH2VpysJSX4WUv9SkFaGe8GBE/kOz\r\nO+PrJvMs1ZOruq4M0B2VB5CzJT2IN5KM5vcW+5cb0xDIRYK4HomLpDnaNitFT84I\r\nuB6JoXF2pnNvLTviAHZFl6iJIurM3qbjPYkCgcEAygFZqcAxgiE7AamK+moiCRe0\r\nbOkw0XYG/JJ8NNu9lBWTSJs2HWzbb5IEHQYf3jLUWE611IZr6kx8IVtTXfBRK4lh\r\nqNGEMjO/wOobiPQbWGCSqcIsvJOwamSSGZ86HndCoR6tviY6CrwH1tfJIfPzfI/O\r\nRvUqe8RdI8G4sfgTNctSMWHU0QWbcn9GuCOmKca4iwp6zQD1BUlnO9UcDYSmzjYl\r\ng4e+D5kllKlfjOiZGZQiKfc8zoc5TBQ2NovKy+LRAoHBALGmdaxpCtCmPoGNRD2s\r\nlLW1uIt4o5CHIufI/dG37VQkdGs32tkeRKjYZzUvr/V6o0tptHjMB4qTZlVP7QWQ\r\ncb9eFoweW663ZWCaU3Uvc4hZpjM+edXdkUB//KcMfAXPcO27KqPbMi4AXVzoYaoz\r\nvzVYdUUp1v3KQg/jr9r3Ly56J1fytIivRectRr1VHHZiTo+clpparEgy0rxOD10V\r\nLKB+uIepCF7b2+/fovJHqbF9Bqdhr8Ds9hA8J3a0ylNx+QKBwHa7BHB5vp+12HDJ\r\nrh4xc3/Upv+8sCztKn4Zu1d2MJ9xbvgmAlCZQwD42eIVT3XSemzNFqKrv5IfEnao\r\nrv9v2MW2IeiP/RtkvzgsYbLFh60SViHhN1o/lFl0c0sMA6UUg90fow84eLUGf7UP\r\nb/C86kg0XhHm1+mXKQdzPZOdM+1SbCUKU4Pnv840cOSaT3gOjTlsF3GXNmy9EChi\r\nwfJQHZrmyHXeux9/7E7MnpS7wgD+yHXSE7NwV4UX/CVjVPcxgQKBwCtWbfK2VU9i\r\npX1jDypTc3M/goZbPhdJt/tpsFgVxGEvewAcljBIC81peSEdTAlwZXOtMCiwJdn9\r\ny0tHN99U0XJAN+I4l0DYzNWG6GwZnABPES+4XlSecJIeX9HUGzh+SNJlRw+Y3N1/\r\ngAEIYPVwFlNN4Ikn0Ph1Hmeqy1rvWDuzPpW7wryKZV9Zc5lYopzE+Vpb2nIOYP01\r\nOjSk1P56AD8mBRxmhh1K+yfKtxXWcVJQkhp2lNaPx2GXAoAaCggDZA==\r\n-----END RSA PRIVATE KEY-----";
            string stringToSign = "CnbIGiYXnA0zmAN8qKpGqjTqtZAScWau" + "|" + xTime;

            if (xSignature != EncryptHelper.Encrypt(stringToSign, privateKey))
            {
                NegativeResponse resp = new NegativeResponse();
                resp.responseCode = "4003302";
                resp.responseMessage = "Invalid Field Format virtualAccountNo";
                return Ok(resp);
            }

            ResponseInquiry res = new ResponseInquiry();
            res.responseCode = "2003200";
            res.responseMessage = "Successful";
            VirtualAccountData virtualAccountData = new VirtualAccountData();
            TotalAmount totalAmount = new TotalAmount();
            AdditionalInfo additionalInfo = new AdditionalInfo();

            virtualAccountData.partnerServiceId = "00088";
            virtualAccountData.customerNo = "081775003";
            virtualAccountData.virtualAccountNo = "00088081775003";
            virtualAccountData.virtualAccountName = "Jokul Doeloe";
            totalAmount.value = "100000000000000.00";
            totalAmount.currency = "IDR";
            additionalInfo.description = "keterangan";

            virtualAccountData.totalAmount = totalAmount;
            virtualAccountData.additionalInfo = additionalInfo;
            res.virtualAccountData = virtualAccountData;

            return Ok(res);

        }

        [HttpPost]
        [Route("snap/v1.0/transfer-va/payment-intrabank")]
        public async Task<IActionResult> CheckPaymentIntrabank([FromBody] RequestPaymentIntra requestPaymentIntra)
        {
            Request.Headers.TryGetValue("X-SIGNATURE", out StringValues xSignature);
            Request.Headers.TryGetValue("X-TIMESTAMP", out StringValues xTime);
            Request.Headers.TryGetValue("X-PARTNER-ID", out StringValues xPartner);
            Request.Headers.TryGetValue("CHANNEL-ID", out StringValues channelId);
            Request.Headers.TryGetValue("X-EXTERNAL-ID", out StringValues xExternalId);
            /*Console.WriteLine("X-SIGNATURE: " + xSignature);
            Console.WriteLine("X-TIMESTAMP: " + xTime);
            Console.WriteLine("X-PARTNER-ID: " + xPartner);
            Console.WriteLine("CHANNEL-ID: " + channelId);
            Console.WriteLine("X-EXTERNAL-ID: " + xExternalId);
            Console.WriteLine("------");
            Console.WriteLine("Request: " + JsonConvert.SerializeObject(requestPaymentIntra));*/

            string privateKey = "MIIG5AIBAAKCAYEAugGR6vgduzYCI/lXaXDeJpsFBwzP5xwrzkiELvw2xSomqTY0\r\nERNpbtNAsnPjHB17lXzMVnEFOvCnw6Aa/fhYMC/UVraNVvq6NWaVjkOXVrx+hlCc\r\n27PvB89zvw54grCm0t3xB0QUSxgHgoyYQpE946HhDa6G2T/kygNjOBEVPD/aHTai\r\n1PkU+sn9jxSbsz4Icqi1wq+srrJISRhrMEXA2AmaoSXtOFSZdnnnksLRqooYLx9f\r\nhzAKlYBYEA5QNk2kwTdwNDO74mIiSydyftqOnx7MKOu0O1mpCfids0Bc2da1BaUI\r\n6eP/4DV3/OIwDcqrCdilciDAR2dmQD35ioTAmBXzRqv9L/GtmGB8ZSPreDGUF41C\r\narfKU6t4I1LD88UAjm1HIQU5FkE/Ca1nLS4fwciaq8NBP5Q1RVSEG4On7GdenUAT\r\nvv966OOKuR3RvvQKjuCdkeRzVBqU1qTEOtanuVlszSB4DQ4sTq3NVoxz3ZBmxKdR\r\nfAONIyoOxT3Bey7ZAgMBAAECggGBALO71qME872112WphzSk0pTdUINadv6r8wxW\r\neBvqrIXiUA9MCA3EAyoKycZmQrslJgjx2YVhmps0dfeLGunz8ktFvFm59mNozUvS\r\nf4tDdtZiN6RT+u41NTEH3aFtOzeVuO2jdkb+GtnhM0iL0IjqTbJK9uvAjB8bJVmq\r\n4lIH98wLKxj1dWxz7zjSu8dpM3aE56bbrR6doDACp+SxysmPbWOUDC3RNyMqZEve\r\nSc0/oVe4LtxB5Y389w01QmLpYcrHRFhSLhUwl2lTL5YccF1hts5H6MRUi8crSzkz\r\nJn/OTYJsSh0X9/TygQikcekMJPh1eQ3Nl7CR/h+B944rPDgSep1bSQsQcLyuO6nd\r\nF7qEcsDYplR7nCc01ExQHgwDwIka3boy9nalfnOtn67RAJdA8RTygEBREnM/dEit\r\n9vnbKLC5yvXw8no/UapKmVdROoyLZ0rddvx7mknbZdm9KosyH1wUsfDWAeKwM3Cm\r\nBYmvwQxrLdNAJ8HebUVaCzIynTXhgQKBwQDruXGXwrXtIOc/3tulLNINlQAyrSnU\r\nJ4NkVvXkMkV8Q7HQ4wLXGGf9X1q1NPZEVmV8Z86gyy4zTBEw4imWOEusOBIXFyGb\r\nmRWRJuuiCNicw8UKlQ/jWDUGFhNj6SXf6eSBH2VpysJSX4WUv9SkFaGe8GBE/kOz\r\nO+PrJvMs1ZOruq4M0B2VB5CzJT2IN5KM5vcW+5cb0xDIRYK4HomLpDnaNitFT84I\r\nuB6JoXF2pnNvLTviAHZFl6iJIurM3qbjPYkCgcEAygFZqcAxgiE7AamK+moiCRe0\r\nbOkw0XYG/JJ8NNu9lBWTSJs2HWzbb5IEHQYf3jLUWE611IZr6kx8IVtTXfBRK4lh\r\nqNGEMjO/wOobiPQbWGCSqcIsvJOwamSSGZ86HndCoR6tviY6CrwH1tfJIfPzfI/O\r\nRvUqe8RdI8G4sfgTNctSMWHU0QWbcn9GuCOmKca4iwp6zQD1BUlnO9UcDYSmzjYl\r\ng4e+D5kllKlfjOiZGZQiKfc8zoc5TBQ2NovKy+LRAoHBALGmdaxpCtCmPoGNRD2s\r\nlLW1uIt4o5CHIufI/dG37VQkdGs32tkeRKjYZzUvr/V6o0tptHjMB4qTZlVP7QWQ\r\ncb9eFoweW663ZWCaU3Uvc4hZpjM+edXdkUB//KcMfAXPcO27KqPbMi4AXVzoYaoz\r\nvzVYdUUp1v3KQg/jr9r3Ly56J1fytIivRectRr1VHHZiTo+clpparEgy0rxOD10V\r\nLKB+uIepCF7b2+/fovJHqbF9Bqdhr8Ds9hA8J3a0ylNx+QKBwHa7BHB5vp+12HDJ\r\nrh4xc3/Upv+8sCztKn4Zu1d2MJ9xbvgmAlCZQwD42eIVT3XSemzNFqKrv5IfEnao\r\nrv9v2MW2IeiP/RtkvzgsYbLFh60SViHhN1o/lFl0c0sMA6UUg90fow84eLUGf7UP\r\nb/C86kg0XhHm1+mXKQdzPZOdM+1SbCUKU4Pnv840cOSaT3gOjTlsF3GXNmy9EChi\r\nwfJQHZrmyHXeux9/7E7MnpS7wgD+yHXSE7NwV4UX/CVjVPcxgQKBwCtWbfK2VU9i\r\npX1jDypTc3M/goZbPhdJt/tpsFgVxGEvewAcljBIC81peSEdTAlwZXOtMCiwJdn9\r\ny0tHN99U0XJAN+I4l0DYzNWG6GwZnABPES+4XlSecJIeX9HUGzh+SNJlRw+Y3N1/\r\ngAEIYPVwFlNN4Ikn0Ph1Hmeqy1rvWDuzPpW7wryKZV9Zc5lYopzE+Vpb2nIOYP01\r\nOjSk1P56AD8mBRxmhh1K+yfKtxXWcVJQkhp2lNaPx2GXAoAaCggDZA==";
            string stringToSign = "CnbIGiYXnA0zmAN8qKpGqjTqtZAScWau" + "|" + xTime;

            if (xSignature != EncryptHelper.Encrypt(stringToSign, privateKey))
            {
                NegativeResponse res = new NegativeResponse();
                res.responseCode = "4003302";
                res.responseMessage = "Invalid Field Format virtualAccountNo";
                return Ok(res);
            }

            PaidAmount paidAmount = new PaidAmount();
            paidAmount.value = "100000.00";
            paidAmount.currency = "IDR";

            VirtualAccountDataPayment vaDataPayment = new VirtualAccountDataPayment();
            vaDataPayment.partnerServiceId = "00088";
            vaDataPayment.customerNo = "081775003";
            vaDataPayment.virtualAccountNo = "00088081775003";
            vaDataPayment.partnerReferenceNo = "2020102900000000000003";
            vaDataPayment.virtualAccountName = "Jokul Doeloe";
            vaDataPayment.paidAmount = paidAmount;
            vaDataPayment.trxDateTime = "2022-01-24T16:39:00+07:00";
            vaDataPayment.paymentRequestId = "1691653236";

            ResponsePayment response = new ResponsePayment();
            response.responseCode = "2003300";
            response.responseMessage = "Successful";
            response.virtualAccountData = vaDataPayment;
            return Ok(response);
        }
    }
}
