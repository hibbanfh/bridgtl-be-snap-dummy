﻿namespace bridgtl_be_snap_dummy.Models
{
    namespace bridgtl_be_snap_dummy.Models
    {
        public class Response
        {
            public string? accessToken { get; set; }
            public string? tokenType { get; set; }
            public string? expiresIn { get; set; }
        }

        public class Grant
        {
            public string? grantType { get; set; }
        }

        public class InquiryIntra
        {
            public string? partnerServiceId { get; set; }
            public string? customerNo { get; set; }
            public string? virtualAccountNo { get; set; }
        }

        public class TotalAmount
        {
            public string? value { get; set; }
            public string? currency { get; set; }
        }

        public class AdditionalInfo
        {
            public string? description { get; set; }
        }

        public class VirtualAccountData
        {
            public string? partnerServiceId { get; set; }
            public string? customerNo { get; set; }
            public string? virtualAccountNo { get; set; }
            public string? virtualAccountName { get; set; }
            public TotalAmount? totalAmount { get; set; }
            public AdditionalInfo? additionalInfo { get; set; }
        }

        public class ResponseInquiry
        {
            public string? responseCode { get; set; }
            public string? responseMessage { get; set; }
            public VirtualAccountData? virtualAccountData { get; set; }
        }

        public class RequestPaymentIntra
        {
            public string? partnerServiceId { get; set; }
            public string? customerNo { get; set; }
            public string? virtualAccountNo { get; set; }
            public string? virtualAccountName { get; set; }
            public string? sourceAccountNo { get; set; }
            public string? partnerReferenceNo { get; set; }
            public string? trxDateTime { get; set; }
            public PaidAmount? paidAmount { get; set; }
        }

        public class PaidAmount
        {
            public string? value { get; set; }
            public string? currency { get; set; }
        }

        public class VirtualAccountDataPayment
        {
            public string? partnerServiceId { get; set; }
            public string? customerNo { get; set; }
            public string? virtualAccountNo { get; set; }
            public string? partnerReferenceNo { get; set; }
            public string? virtualAccountName { get; set; }
            public PaidAmount? paidAmount { get; set; }
            public string? trxDateTime { get; set; }
            public string? paymentRequestId { get; set; }

        }

        public class ResponsePayment
        {
            public string? responseCode { get; set; }
            public string? responseMessage { get; set; }
            public VirtualAccountDataPayment? virtualAccountData { get; set; }
        }

        public class NegativeResponse
        {
            public string? responseCode { get; set; }
            public string? responseMessage { get; set; }
        }
    }

}
