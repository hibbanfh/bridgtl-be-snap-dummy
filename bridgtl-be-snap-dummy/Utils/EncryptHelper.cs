﻿using System.Security.Cryptography;
using System.Text;

namespace bridgtl_be_snap_dummy.Utils
{
    public class EncryptHelper
    {
        public static string Encrypt(string textToEncrypt, string privateKey)
        {
            using RSA rsa = RSA.Create();
            if (privateKey.Contains("BEGIN RSA PRIVATE KEY"))
            {
                privateKey = privateKey.Replace("-----BEGIN RSA PRIVATE KEY-----", "")
                                       .Replace("-----END RSA PRIVATE KEY-----", "")
                                       .Replace("\n", "")
                                       .Replace("\r", "");

                byte[] dataBytes = Convert.FromBase64String(privateKey);
                rsa.ImportRSAPrivateKey(dataBytes, out _);
            }

            byte[] dataToSign = Encoding.UTF8.GetBytes(textToEncrypt);
            byte[] signatureBytes = rsa.SignData(dataToSign, HashAlgorithmName.SHA256, RSASignaturePadding.Pkcs1);

            return Convert.ToBase64String(signatureBytes); ;
        }
    }
}
