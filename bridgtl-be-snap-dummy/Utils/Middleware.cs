﻿namespace bridgtl_be_snap_dummy.Utils
{
    public class Middleware
    {
        private readonly RequestDelegate _next;

        public Middleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            foreach (var header in context.Request.Headers)
            {
                if(header.Key == "X-SIGNATURE" || header.Key == "X-TIMESTAMP" || header.Key == "X-PARTNER-ID" || header.Key == "CHANNEL-ID" || header.Key == "X-EXTERNAL-ID")
                {
                    Console.WriteLine($"Request Header: {header.Key} - {header.Value}");
                }
            }

            var requestBody = await ReadRequestBody(context.Request);
            Console.WriteLine($"Request {context.Request.Method} {context.Request.Path}{context.Request.QueryString} - Body: {requestBody}");

            var originalBodyStream = context.Response.Body;
            using var responseBody = new MemoryStream();
            context.Response.Body = responseBody;

            await _next(context);

            responseBody.Seek(0, SeekOrigin.Begin);
            var responseBodyContent = await new StreamReader(responseBody).ReadToEndAsync();
            Console.WriteLine($"Response: {context.Response.StatusCode} - Body {responseBodyContent}");

            responseBody.Seek(0, SeekOrigin.Begin);
            await responseBody.CopyToAsync(originalBodyStream);
        }

        public async Task<string> ReadRequestBody(HttpRequest request)
        {
            using var reader = new StreamReader(request.Body);
            return await reader.ReadToEndAsync();
        }
    }
}
